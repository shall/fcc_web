$(document).ready(function () {
  $("#btn_sum").click(function () {
    var meh = [1, 22, 13, 4, 6, 7, 11, 3];
    $("#result_sum").text(sumOfArray(meh));
  });
  $("#btn_jagged").click(function () {
    var woot = [[1, 2], [3, 4], [5, 6, 7]];
    $("#result_jagged").text(multiplyAll(woot));
  });
});

function sumOfArray(arr) {
  var j = 0;
  for (var i = 0; i < arr.length; i++) {
    j += arr[i];
  }
  return j;
}

function multiplyAll(arr) {
  var product = 1;
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
      product *= arr[i][j];
    }
  }
  return product;
}

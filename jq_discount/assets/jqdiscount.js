$(document).ready(function() {
    $("#filters").on("click", ".on-sale", function() {
        $(".highlight").removeClass("highlight");
        $(".tour").filter(".on-sale").addClass("highlight");
    });

    $("#filters").on("click", ".featured", function() {
        $(".highlight").removeClass("highlight");
        $(".tour").filter(".featured").addClass("highlight");
    });

    $(".tour").on("mouseenter", function() {
        $(this).addClass("bg-highlight");
        $(this).find(".photos").show("fast");
        $(this).find(".per-night").animate({
            "top": "-60px",
            "opacity": "1"
        }, "fast");
    });

    $(".tour").on("mouseleave", function() {
        $(this).removeClass("bg-highlight");
        $(this).find(".photos").hide("fast");
        $(this).find(".per-night").animate({
            "top": "-50",
            "opacity": "0"
        }, "fast");
    });

    $(".tour").on("click", "button", function() {
        var tour = $(this).closest(".tour");
        var discount = tour.data("discount");
        var message = $("<span>Call 1-555-winning-meme for a $" + discount + " discount.</span>");
        tour.append(message);
        $(this).remove();
    });
});
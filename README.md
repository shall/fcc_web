# freeCodeCamp practice

**Not yet organized.**

Just a repo for some of the practice/lessons for freeCodeCamp, a boot camp for beginning web developers. In short, it
consists of 100 hours of fundamentals (HTML, CSS, JavaScript, the MEAN stack) and around 1,600 hours of pair programming
and real-world projects for non-profits.

Quite a few of these files will be missing assets that are local to a different environment. I'll clean them up as I go, 
but for the most part I intend to leave things "unimproved" to refer back to them later (for personal historical
interest if nothing else).

## Resources used for this collection
This is updated _infrequently._
* [freeCodeCamp](http://www.freecodecamp.com/): Learn webdev, assist non-profits
* [Codepen](http://codepen.io): HTML, JS, and CSS real-time editing and experimentation environment. Makes it easy to
try things out, share mockups, and export to Github or .zip archive.

## Outdated resources (no longer used by FCC)
Many of these were used a while ago, back in the days of yore when FCC used them as part of the course.
* [Dash](https://dash.generalassemb.ly): Interactive excercises for HTML/CSS/jQuery *no longer used by FCC AFAIK*
* [CodeSchool](https://www.codeschool.com/): Interactive lessons in a variety of webdev technologies *no longer used by
FCC AFAIK*
* [CodeCademy](http://www.codecademy.com/): Interactive lessons, mostly useful as a guided exposure to syntax *no longer
used by FCC AFAIK*

## Contents
These mini-projects are the result of following the freeCodeCamp challenges. Some of them are just "stock"
implementations of the course. A few have additional features. Not everything from the course is here: the earliest
lessons aren't really deserving of a spot by themselves.
